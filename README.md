# latex-SQL-Digram
- import package : \usepackage{sqldiagram}
## DOCUMENTATION - SQL
- You can put SQL code with colorization
- You can pot bash line with colorization (for the results of codes executed in shell)
```tex
\begin{SQLFrame}
    <your SQL code>
\end{SQLFrame}
```
```tex
\begin{BashFrame}
    <your bash result>
\end{BashFrame}
``` 

## DOCUMENTATION - SQL Schema
#### Use SQL Algèbre Relationnel :
- create SQL Relationel Algebra Diagram :  \begin{AR}{FIGURE_TITLE | empty} ... \end{AR}
- States :
    - \relation {VAR_NAME} {TEXT}{POSITION};
    - \selection{VAR_NAME}{TEXT}{POSITION};
    - \restrict{VAR_NAME}{TEXT}{POSITION};
    - \join{VAR_NAME}{TEXT}{POSITION};
    - \ljoin{VAR_NAME}{TEXT}{POSITION};
    - \rjoin{VAR_NAME}{TEXT}{POSITION};
    - \group{VAR_NAME}{TEXT}{POSITION};
    - \union{VAR_NAME}{TEXT}{POSITION};
    - \intersect{VAR_NAME}{TEXT}{POSITION};
    - \minus{VAR_NAME}{TEXT}{POSITION};

- Links : \arrow{STATE_VAR_NAME_1}{STATE_VAR_NAME_2};

POSITION in above | below | right | left = of VAR_NAME or ∅

##### Example :
```tex
\begin{AR}{Test figure}
    \relation {a} {COMMANDES} {};
    \selection{b}{nomJ}{right=of a};
    \restrict{c}{restr}{below=of b};
    \join{d}{join}{below=of c};
    \ljoin{e}{ljoin}{left=of d};
    \rjoin{f}{rjoin}{right=of d};
    \group{g}{group}{below=of d};
    \union{h}{union}{below right=of g};
    \intersect{i}{inter}{below=of h};
    \minus{j}{min}{below=of i};
  
    \arrow{a}{b};
\end{AR}
```

#### Use SQL UML :
- create SQL Relationel Algebra Diagram :  \begin{UML}{FIGURE_TITLE | empty} ... \end{UML}

- Class :
    - \class{VAR_NAME}{NAME_CLASS}{ATTRIBUT}{POSITION};
    - \lowclass{VAR_NAME}{NAME_CLASS}{ATTRIBUT}{POSITION};
- Relation :
    - \lca{CLASS1}{CARDINALITY_CLASS1}{CLASS2};
    - \card{CLASS1}{CARDINALITY_CLASS1}{CLASS2}{CARDINALITY_CLASS2};
    - \aca{CLASS1}{CARDINALITY_CLASS1}{CLASS2}{CARDINALITY_CLASS2}{CLASS3};
    - \herit{CLASS1}{CLASS2};

POSITION in above | below | right | left = of VAR_NAME or ∅

ATTRIBUT in \primary{ATT_NAME} | \att{ATT_NAME} | ∅

##### Example :
```tex
\begin{UML}{Question \getcurrentref{section}}
    \class{T}{One}{\primary{id}}{};
    \lowclass{T2}{Two}{\primary{id} \att{labelle}}{below=of T};
    \class{T3}{Three}{\primary{id}}{right=of T2};
    \class{T4bis}{FourChild}{\primary{id}}{below=of T2};
    \class{T5}{Five}{\primary{id}}{below=of T3};
    \class{T4}{Four}{}{left=of T4bis};

    \lca{T2}{1..*}{T};
    \card{T2}{1..*}{T3}{*};
    
    \aca{T2}{*}{T4bis}{*}{T5};

    \herit{T4}{T4bis};
\end{UML}
```